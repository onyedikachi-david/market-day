'reach 0.1';

const commonInteract = {
  reportExit: Fun([], Null)
};

const sellerInteract = {
  ...commonInteract,
  reportReady: Fun([], Null)
};

const buyerInteract = {
  ...commonInteract
};

export const main = Reach.App(() => {
  const S = Participant('Seller', sellerInteract);
  const B = Participant('Buyer', buyerInteract);
  deploy();

  S.publish();
  S.interact.reportReady();
  commit();

  B.publish();
  commit();

  each([S, B], () => interact.reportExit());
  exit();
});